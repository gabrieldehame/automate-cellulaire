from tkinter import *
import numpy as np
import time
from random import randint
#Conditions initiales, dépendent de l'autre partie du TIPE
l=np.ones((500,500))
l[2][40]=0




n=len(l)

def start():
    for i in range (n):
        for j in range (n):
            if l[i][j]==0:
                canvas.create_rectangle(j*1000/n,i*1000/n,(j+1)*1000/n,(i+1)*1000/n,fill="black",outline="black")

def stop():
    global s
    s=0

def nb_arbre_intact():
    c=0
    for i in range (n):
        for j in range (n):
            if l[i][j]==2: c+=1
    return(n*n-c)

def nb_voisins_feu(i,j,copy):
    count=0
    for k in range (max(i-1,0),min(n,i+2)):
        for q in range (max(0,j-1),min(n,j+2)):
            if (not(k==i and q==j))and copy[k][q]==2: count+=1
    return count

def boucle():
    if s!=0 and nb_arbre_intact()!=0:
        c=[]
        copy=[]
        for i in range (n):
            t=[]
            tcopy=[]
            for j in range (n):
                tcopy.append(l[i][j])
                if l[i][j]==2:
                    t.append([i,j])
            if len(t)!=0: c.append(t)
            copy.append(tcopy)
            lenc=len(c)
        for i in range (lenc):
            lenc2=len(c[i])
            for j in range (lenc2):
                l[c[i][j][0]][c[i][j][1]]=3
                canvas.create_rectangle(1000*c[i][j][1]/n,1000*c[i][j][0]/n,1000*(c[i][j][1]+1)/n,1000*(c[i][j][0]+1)/n,fill="grey",outline="grey")
                for k in range (max(0,c[i][j][0]-1),min(n,c[i][j][0]+2)):
                    for w in range (max(0,c[i][j][1]-1),min(n,c[i][j][1]+2)):
                        if not(k==c[i][j][0] and w==c[i][j][1]) and copy[k][w]==1 and nb_voisins_feu(k,w,copy)!=0:
                            l[k][w]=2
                            canvas.create_rectangle(1000*w/n,1000*k/n,1000*(w+1)/n,1000*(k+1)/n,fill="red",outline="red")
                for k in range (c[i][j][0]-v,c[i][j][0]+v+1):
                    for w in range (max(c[i][j][1]-v,0),min(n,c[i][j][1]+v+1)):
                        if not(k==c[i][j][0] and w==c[i][j][1]):
                            propagation_aerienne(k,w,copy)
        window.after(250,boucle)
    else:
        return()
def propagation_aerienne(i,j,copy):
    if v!=0:
        if direction=="S":
            for k in range (j-1,j+2):
                if k<=n-1 and k>=0 and i-v>=0 and copy[i-v][k]==2:
                    tset=randint(1,20)
                    if tset==1:
                        l[i][j]=2
                        canvas.create_rectangle(1000*j/n,1000*i/n,1000*(j+1)/n,1000*(i+1)/n,fill="red",outline="red")
        if direction=="N":
            for k in range (j-1,j+2):
                if k<=n-1 and k>=0 and i+v<=n-1 and copy[i+v][k]==2:
                    tset=randint(1,20)
                    if tset==1:
                        l[i][j]=2
                        canvas.create_rectangle(1000*j/n,1000*i/n,1000*(j+1)/n,1000*(i+1)/n,fill="red",outline="red")
        if direction=="E":
            for k in range (i-1,i+2):
                if k<=n-1 and k>=0 and j-v>=0 and copy[k][j-v]==2:
                    tset=randint(1,20)
                    if tset==1:
                        l[i][j]=2
                        canvas.create_rectangle(1000*j/n,1000*i/n,1000*(j+1)/n,1000*(i+1)/n,fill="red",outline="red")
        if direction=="O":
            for k in range (i-1,i+2):
                if k<=n-1 and k>=0 and j+v<=n-1 and copy[k][j+v]==2:
                    tset=randint(1,20)
                    if tset==1:
                        l[i][j]=2
                        canvas.create_rectangle(1000*j/n,1000*i/n,1000*(j+1)/n,1000*(i+1)/n,fill="red",outline="red")
        if direction=="SE":
            if (i-v>=0 and j-v>=0 and copy[i-v][j-v]==2) or (i-v>=0 and j-v+1>=0 and copy[i-v][j-v+1]==2) or (j-v>=0 and i-v+1>=0 and copy[i-v+1][j-v]==2):
                tset=randint(1,20)
                if tset==1:
                    l[i][j]=2
                    canvas.create_rectangle(1000*j/n,1000*i/n,1000*(j+1)/n,1000*(i+1)/n,fill="red",outline="red")
        if direction=="NO":
            if (i+v<=n-1 and j+v<=n-1 and copy[i+v][j+v]==2) or (i+v<=n-1 and j+v-1<=n-1 and copy[i+v][j+v-1]==2) or (j+v<=n-1 and i+v-1<=n-1 and copy[i+v-1][j+v]==2):
                tset=randint(1,20)
                if tset==1:
                    l[i][j]=2
                    canvas.create_rectangle(1000*j/n,1000*i/n,1000*(j+1)/n,1000*(i+1)/n,fill="red",outline="red")
        if direction=="NE":
            if (i+v<=n-1 and j-v>=0 and copy[i+v][j-v]==2) or (i+v<=n-1 and j-v+1>=0 and copy[i+v][j-v+1]==2) or (j-v>=0 and i+v-1<=n-1 and copy[i+v-1][j-v]==2):
                tset=randint(1,20)
                if tset==1:
                    l[i][j]=2
                    canvas.create_rectangle(1000*j/n,1000*i/n,1000*(j+1)/n,1000*(i+1)/n,fill="red",outline="red")
        if direction=="SO":
            if (i-v>=0 and j+v<=n-1 and copy[i-v][j+v]==2) or (i-v>=0 and j+v-1<=n-1 and copy[i-v][j+v-1]==2) or (j+v<=n-1 and i-v+1>=0 and copy[i-v+1][j+v]==2):
                tset=randint(1,20)
                if tset==1:
                    l[i][j]=2
                    canvas.create_rectangle(1000*j/n,1000*i/n,1000*(j+1)/n,1000*(i+1)/n,fill="red",outline="red")


def reprendre():
    global s
    s=1
    boucle()

def ajout_foyer():
    x=int(e1.get())
    y=int(e2.get())
    l[y][x]=2
    canvas.create_rectangle(1000*x/n,1000*y/n,1000*(x+1)/n,1000*(y+1)/n,fill="red", outline="red")
def vent_fort():
    global v
    v=4
def vent_intermediaire():
    global v
    v=2
def vent_faible():
    global v
    v=0
def dir_vent():
    global direction
    direction=e3.get()

def reset():
    global v
    global s
    v=0
    s=1
    for i in range (n):
        for j in range (n):
            if l[i][j]!=0 and l[i][j]!=1:
                l[i][j]=1
                canvas.create_rectangle(1000*j/n,1000*i/n,1000*(j+1)/n,1000*(i+1)/n,fill="green", outline="green")

s=1
v=0
window = Tk()
parameters = Tk()
window.title("Simulation de feu de forêt")
parameters.title("Paramètres")
canvas= Canvas(window,width=1000,height=1000,background="green")
canvas.pack()
l1=Label(parameters, text="Abscisse d'un foyer(0-"+str(n-1)+")")
l1.pack()
e1=Entry(parameters, width=3, bg="grey")
e1.pack()
l2=Label(parameters, text="Ordonnée d'un foyer(0-"+str(n-1)+")")
l2.pack()
e2=Entry(parameters, width=3, bg="grey")
e2.pack()
b4=Button(parameters, text="Valider",command=ajout_foyer)
b4.pack()
l3=Label(parameters,text="Force du vent:")
l3.pack()
b5=Button(parameters, text="Forte",command=vent_fort)
b5.pack()
b6=Button(parameters, text="Intermédiaire",command=vent_intermediaire)
b6.pack()
b7=Button(parameters, text="Faible",command=vent_faible)
b7.pack()
l4=Label(parameters,text="Direction du vent (N,S,E,O,NE,SE,NO,SO) (nord vers le haut):")
l4.pack()
e3=Entry(parameters,width=2,bg="grey")
e3.pack()
b8=Button(parameters, text="Valider direction",command=dir_vent)
b8.pack()
b9=Button(parameters, text="Reset",command=reset)
b9.pack()
b1=Button(parameters,text="Démarrer",command=boucle)
b1.pack()
b2=Button(parameters,text="Arrêter",command=stop)
b2.pack()
b3=Button(parameters,text="Reprendre",command=reprendre)
b3.pack()
start()
window.mainloop()
